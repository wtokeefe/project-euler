# project-euler

My solutions to Project Euler problems in whatever languages take my fancy. I'm
using this to teach myself git and to learn the basics of the languages used.

I'll iterate on the code here if I think I can make it better.

## C

I'm writing C with the GNU C Manual as a reference, although I generally avoid
knowingly using GCC extensions for portability's sake.

I'm still not 100% sure how to style my C code, but I think I'm close to what I
consider optimal. 

Test with `gcc file.c -o file; ./file`

## Scheme

I'm writing (fairly poor) R7RS Scheme and building it using Chicken (R5RS) with
my fingers crossed.

Test with `csc file.scm; ./file`

## Julia

Julia is the first language I stuck out any Project Euler problems with, so most
 of my other solutions are based on the Julia ones. The comments are generally
much less informative because I was learning as I went, rather than beforehand.

Test with `julia -L file.jl` then run the function manually.

When I was writing them I tested using LightTable's inline evaluation. Since I
switched to Emacs I've mostly just been using `julia -L file.jl` then running
the functions myself, even though I'm sure Emacs has a good way of doing that.

## Fortran 90

I really have no idea what I'm doing with this language.

Test with `gfortran file.f90 -o file; ./file`

