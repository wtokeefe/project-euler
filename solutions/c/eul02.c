/*  Walter O'Keefe
 *  Created: 2014-11-24
 *
 *  Project Euler, Problem 02:
 *  By considering the terms in the Fibonacci sequence whose values do 
 *  not exceed four million, find the sum of the even-valued terms.
 *
 *  Code answer: 4613732 [Correct]
 *  >> You are the 338575th person to have solved this problem.
 *
 * TODO: 1. Maybe, someday, do this with arrays and separate functions.
 */

#include <stdio.h>
#include "euler.h"

int main (void)
{
  int f, i, total = 0, max = 4000000;

  for (i = 0; f < max; i++)
    {
      f = fib (i);
      if (f % 2 == 0)
	total += f;
    }
    
  // Output
  printf ("%d\n", total);
  return 0;
}
