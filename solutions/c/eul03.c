/* Walter O'Keefe
 * Project Euler, problem 03
 * Created 2015-07-16
 *
 * Find the largest prime factor of 600851475143.
 */

/* -- Header -- */

#include <stdio.h>
#include <math.h>
#include "euler.h"

int eul03 (unsigned long long int n);

/* -- Solution -- */

int main (void)
{
  unsigned long long int n = 600851475143; // big number!
  int out = eul03 (n);  

  printf ("%d\n", out);

  return 0;
}

/* Find largest prime factor of n */
int eul03 (unsigned long long int n) // n for numerator
{
  long int d, ans = 0; // d for denominator

  for (d = 1; d < sqrt (n); d++) 
    if ((drem (n, d) == 0) && is_prime (d))
      ans = d;
  
  return ans;
}
