/* This is good enough, right? */
import std.stdio;

int main ()
{
  int answer;
  int[] test_factors = [ 3, 5 ];

  foreach (i; 0 .. 1000)
    answer += eul01 (i, test_factors);
  
  writeln(answer);
  return 0;
}

/* Return true if n has a number in test_factors as a factor. */
bool has_factor_in_array (int n, int[] test_factors)
{
  foreach (f; test_factors)
    if (n % f == 0)
      return true;
  
  return false;
}

/* Return n if n has a number in test_factors as a factor, else 0. */
int eul01 (int n, int[] test_factors)
{
  return (has_factor_in_array (n, test_factors) ? n : 0);
}

