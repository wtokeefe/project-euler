import std.stdio;

int main ()
{
  int f, total, max = 4000000;

  for (int i; f < max; ++i)
    {
      f = fib (i);
      (f % 2 == 0) && (total += f);
    }

  writeln (total);
  return 0;
}

/* Return nth fibonacci number */
int fib (int n)
{
  if (n < 2)
    return n;
  if (n == 2)
    return 1;

  struct fibs
  {
    int older, old, current;
  }

  auto f = fibs (0, 1, 1);

  foreach (int i; 2 .. n)
    {
      f.older = f.old;
      f.old = f.current;
      f.current = f.old + f.older;
    }

  return f.current;
}
