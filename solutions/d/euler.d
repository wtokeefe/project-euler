module euler;

/* Return true if n has a number in test_factors as a factor. */
bool has_factor_in_array (int n, int[] test_factors)
{
  foreach (f; test_factors)
    if (n % f == 0)
      return true;
  
  return false;
}

