#|
#   Walter O'Keefe
#   Project Euler, problem 01:
#   Find the sum of all integers below 1000 with a factor of 3 or 5
|#

;; return the first number in l that is a factor of num, else #f.
;; 1 doesn't count as a factor, but num does.
(define member-factor
  (lambda (num l)
    (if (= 0 num) #f
	(cond ((null? l) #f)
	      ((and (not (<= (car l) 1))
		    (= 0 (modulo num (car l)))) (car l))
	      (else (member-factor num (cdr l)))))))

(define eul01
  (lambda (max fac)
    (let loop ((i 0) (sum 0))
      (cond ((= i max) sum)
	    ((member-factor i fac) (loop (+ i 1) (+ sum i)))
	    (else (loop (+ i 1) sum))))))

(begin (display (eul01 1000 '(3 5))) (newline))
