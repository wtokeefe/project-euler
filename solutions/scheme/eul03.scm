;; Created: 2015-05-26
;; Updated: 2015-06-13 when I should be revising for exams

;; Project Euler, problem 03:
;;   The prime factors of 13195 are 5, 7, 13 and 29.
;;   What is the largest prime factor of the number 600851475143 ?

;; My Julia solution relied on Julia's libraries, so I guess I'll
;; actually have to think about this.

;; Basic trial division
(define prime?
  (lambda (n)
    (let loop ((d 3) (max (sqrt n)))
      (if (even? n) #f
	  (cond ((> d max) #t)
		((= 0 (modulo n d)) #f)
		(else (loop (+ d 2) max)))))))

;; Find the largest prime factor of a number
(define largest-prime-factor
  (lambda (n) ; numerator
    (let loop ((d 1) (ans 0))
      (cond ((>= d (sqrt n)) ans)
	    ((and (= 0 (modulo n d)) (prime? d))
	     (loop (+ d 1) d))
	    (else (loop (+ d 1) ans))))))


(begin (display "Enter n: ")
       (display (largest-prime-factor (read)))
       (newline))
